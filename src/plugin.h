/* gmpc-mdcover2 (GMPC plugin)
 * Copyright (C) 2007-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#define META_ALBUM_RULE  "if \"%artist%\" >= \"L\"\n"\
                "   then\n"\
                "      put \"L-Z\"\n"\
                "   else\n"\
                "      put \"0-K\""\
                "fi\n"\
                "put \"/\"\n"\
                "put \"%artist%\"1\n"\
                "put \"/%artist%\"\n"\
                "put \"/\"\n"\
                "if \"%date%\" != \"\"\n"\
                "    Then\n"\
                "    put \"%date% - \"\n"\
                "fi\n"\
                "put \"%album%\""\
                "put \"/\"\n"\
                "put \"%album%.jpg\"\n"
#define META_ARTIST_RULE  "if \"%artist%\" >= \"L\"\n"\
                "   then\n"\
                "      put \"L-Z\"\n"\
                "   else\n"\
                "      put \"0-K\""\
                "fi\n"\
                "put \"/\"\n"\
                "put \"%artist%\"1\n"\
                "put \"/%artist%\"\n"\
                "put \"/\"\n"\
                "put \"%artist%.jpg\"\n"
#define META_ALBUM_TXT_RULE  "if \"%artist%\" >= \"L\"\n"\
                "   then\n"\
                "      put \"L-Z\"\n"\
                "   else\n"\
                "      put \"0-K\""\
                "fi\n"\
                "put \"/\"\n"\
                "put \"%artist%\"1\n"\
                "put \"/%artist%\"\n"\
                "put \"/\"\n"\
                "if \"%date%\" != \"\"\n"\
                "    Then\n"\
                "    put \"%date% - \"\n"\
                "fi\n"\
                "put \"%album%\""\
                "put \"/\"\n"\
                "put \"%album%.txt\"\n"
#define META_ARTIST_TXT_RULE  "if \"%artist%\" >= \"L\"\n"\
                "   then\n"\
                "      put \"L-Z\"\n"\
                "   else\n"\
                "      put \"0-K\""\
                "fi\n"\
                "put \"/\"\n"\
                "put \"%artist%\"1\n"\
                "put \"/%artist%\"\n"\
                "put \"/\"\n"\
                "put \"BIOGRAPHY\"\n"
