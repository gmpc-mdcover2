/* gmpc-mdcover2 (GMPC plugin)
 * Copyright (C) 2007-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <regex.h>
#include <gmpc/plugin.h>
#include <libmpd/libmpd.h>
#include <libmpd/debug_printf.h>
#include <config.h>
#include "plugin.h"
#include "parser.h"

int fetch_get_image(mpd_Song *song,MetaDataType type, char **path);

void music_dir_cover_art_pref_construct(GtkWidget *container);
void music_dir_cover_art_pref_destroy(GtkWidget *container);
GList * fetch_cover_art_path_list(mpd_Song *song);
int fetch_cover_art_path(mpd_Song *song, gchar **path);
static GtkWidget *wp_pref_vbox = NULL;


/*
 * Enable/Disable state of the plugins
 */
static int mdca_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, "music-dir-cover2", "enable", TRUE);
}
static void mdca_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "music-dir-cover2", "enable", enabled);
}
/* priority */
static int fetch_cover_priority()
{
				return cfg_get_single_value_as_int_with_default(config, "music-dir-cover2", "priority", 5);
}
/* Plugin structure(s) */ 
gmpcPrefPlugin mdca_pref = {
	music_dir_cover_art_pref_construct,
	music_dir_cover_art_pref_destroy
};

gmpcMetaDataPlugin mdca_cover = {
	fetch_cover_priority,
	fetch_get_image
};
/* a workaround so gmpc has some sort of api checking */
int plugin_api_version = PLUGIN_API_VERSION;
/* the plugin */
gmpcPlugin plugin = {
	.name           = "Music Dir Fetcher2",
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_META_DATA,
	.pref           = &mdca_pref, /* preferences */
	.metadata       = &mdca_cover, /* meta data */
	.get_enabled    = mdca_get_enabled,
	.set_enabled    = mdca_set_enabled
};


int fetch_get_image(mpd_Song *song,MetaDataType type, char **path)
{

	if(song  == NULL )
	{
		return META_DATA_UNAVAILABLE;
	}
	else if(type == META_ALBUM_ART || type == META_ARTIST_ART || type == META_ARTIST_TXT)
	{
		gchar *musicroot= cfg_get_single_value_as_string(config, "music-dir-cover2", "musicroot");
        gchar *markup;
        if(type == META_ALBUM_ART)
        {
            markup= cfg_get_single_value_as_string_with_default(config, "music-dir-cover2", "album-art-format",META_ALBUM_RULE);
        } else if (type == META_ARTIST_ART)
        {
            markup= cfg_get_single_value_as_string_with_default(config, "music-dir-cover2", "artist-art-format",META_ARTIST_RULE);
        } else if (type == META_ARTIST_TXT)
        {
            markup= cfg_get_single_value_as_string_with_default(config, "music-dir-cover2", "artist-txt-format",META_ARTIST_TXT_RULE);
        } else if (type == META_ALBUM_TXT)
        {
            markup= cfg_get_single_value_as_string_with_default(config, "music-dir-cover2", "album-txt-format",META_ALBUM_TXT_RULE);
        }
        if(musicroot && markup)
        {
            GString *string = g_string_new(musicroot);
            g_string_append(string, "/");
            parse_string(string, song, markup);
            printf("Trying: %s\n", string->str);
			if(g_file_test(	string->str, G_FILE_TEST_EXISTS))
			{
				*path = string->str;
                g_string_free(string, FALSE);
                g_free(musicroot);
                g_free(markup);
				return META_DATA_AVAILABLE;
			}
            g_string_free(string, TRUE);
		}
        g_free(musicroot);
        g_free(markup);
	}

	return META_DATA_UNAVAILABLE;
}

void music_dir_cover_art_enable_toggle(GtkWidget *wid)
{
	int kk = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(wid));
	cfg_set_single_value_as_int(config, "music-dir-cover2", "enable", kk);
}
void music_dir_cover_art_pref_destroy(GtkWidget *container)
{
	gtk_container_remove(GTK_CONTAINER(container), wp_pref_vbox);
}
static void info_entry_edited(GtkWidget *entry)
{
	const char *str = gtk_entry_get_text(GTK_ENTRY(entry));
	if(str)
	{
		cfg_set_single_value_as_string(config, "music-dir-cover2", "musicroot",(char *)str);
	}
}

void music_dir_cover_art_pref_construct(GtkWidget *container)
{
	GtkWidget *enable_cg = gtk_check_button_new_with_mnemonic("_Enable mpd's music dir as cover art source");
	GtkWidget *entry = NULL;
	char *entry_str = cfg_get_single_value_as_string(config, "music-dir-cover2", "musicroot");
	wp_pref_vbox = gtk_vbox_new(FALSE,6);

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enable_cg), 	
			cfg_get_single_value_as_int_with_default(config, "music-dir-cover2", "enable", TRUE));

	g_signal_connect(G_OBJECT(enable_cg), "toggled", G_CALLBACK(music_dir_cover_art_enable_toggle), NULL);
	gtk_box_pack_start(GTK_BOX(wp_pref_vbox), enable_cg, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(container), wp_pref_vbox);

	entry = gtk_entry_new();
	if(entry_str)
	{
		gtk_entry_set_text(GTK_ENTRY(entry), entry_str);
		cfg_free_string(entry_str);
	}
	gtk_box_pack_start(GTK_BOX(wp_pref_vbox), gtk_label_new("Music Root:"), FALSE, FALSE,0);
	gtk_box_pack_start(GTK_BOX(wp_pref_vbox), entry, FALSE, FALSE,0);
	g_signal_connect(G_OBJECT(entry), "changed", G_CALLBACK(info_entry_edited), NULL);

	gtk_widget_show_all(container);
}
