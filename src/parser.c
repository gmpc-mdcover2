/* gmpc-mdcover2 (GMPC plugin)
 * Copyright (C) 2007-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <libmpd/libmpd.h>
char * parser(char **iter,GString *string);

#define NEXT_ENTRY(a) while((*a) == ' ' || (*a) == '\n') (a)++
int evaluate_switch_rule(char *start)
{
    char *left = NULL, *right = NULL;
    char *f = start;
    int stack = 0;
    /* skip white space */
    while(*start == ' ' || *start == '\n') start++;
    if(*start == '"'){ stack++; start++;}
    /* Get left */
    left = start;
    do{
        if(*start == '"')
            stack--;
        else
            start++;
    }while(stack || (*start != ' ' && *start != '"') );
    *start = '\0';
    start++;
    /* get separator */
    /* skip white space */
    while(*start == ' ' || *start == '\n') start++;    
    f = start;
    while(*start != ' ' ) start++;    
    *start = '\0';
    start++;

    /* skip white space */
    while(*start == ' ' || *start == '\n') start++;
    if(*start == '"'){ stack++; start++;}
    /* Get right*/
    right= start;
    do{

        if(*start == '"')
            stack--;
        else
            start++;
    }while(stack || (*start != ' ' && *start != '"') );
    *start = '\0';

    int retv = 0;
    if(!strcmp(f, ">")) {
        retv = (strcmp(left, right) > 0);
    } else if(!strcmp(f, ">=")) {
        retv = (strcmp(left, right) >= 0);
    } else if(!strcmp(f, "<")) {
        retv = (strcmp(left, right) < 0);
    } else if(!strcmp(f, "<=")) {
        retv = (strcmp(left, right) <= 0);
    } else if (!strcmp(f, "==")) {
        retv = (strcmp(left, right) == 0);
    } else if (!strcmp(f, "!=")) {
        retv = (strcmp(left, right) != 0);
    }

    return retv;
}
char *if_parser(char **iter,GString *string)
{
    char *start = *iter;
    while(**iter != '\0' && strncasecmp(*iter, "then", 4))
        (*iter)++;
    **iter = '\0';
    (*iter)+=4;
    /** Parse the switch rule here */

    int eval = evaluate_switch_rule(start);
    if(eval)
    {
        (*iter) = parser(iter,string);
        NEXT_ENTRY(*iter);
        while(**iter != '\0' && strncasecmp(*iter, "fi",2) && strncasecmp(*iter, "else",4))
        {
            (*iter) = parser(iter,string);
        }
    }
    else
    {
        start = *iter;
        while(**iter != '\0' && strncasecmp(*iter, "else", 4) && strncasecmp(*iter, "fi",2))
            (*iter)++;
        if(!strncasecmp(*iter, "else",3))
        {
            (*iter)+=4;
            (*iter) = parser(iter,string);
            NEXT_ENTRY(*iter);
            while(**iter != '\0' && strncasecmp(*iter, "fi",2))
            {
                (*iter) = parser(iter,string);
            }
        }
    }
    NEXT_ENTRY(*iter);
    while(**iter != '\0' && strncasecmp(*iter, "fi",2))
     (*iter)++;
           

    if(!strncasecmp(*iter, "fi", 2))
    {
        (*iter)+=2;
    }

    return  *iter;
}

char *put_parser(char **iter,GString *string)
{
    char *start = *iter;
    int stack = 0;
    char *value;
    int num = -1;
    /* skip white space */
    while(*start == ' ' || *start == '\n') start++;
    if(*start == '"'){ stack++; start++;}
    /* Get right*/
    value= start;
    do{
        start++;
        if(*start == '"')
            stack--;
    }while(stack || (*start != ' ' && *start != '"') );
    *start = '\0';

    num = atoi((start)+1);
    if(num > 0)
    {
        g_string_append_printf(string,"%.*s",num, value); 
        /* Skip over the start of the number */
        start++;
        /* walk skip numbers */ 
        while((*start) >= '0' && (*start) <= '9') start++;
    }
    else
        g_string_append(string, value); 

    return start;
}

char * parser(char **iter, GString *string)
{
    /* skip leading spacees */
    NEXT_ENTRY(*iter);
    if(**iter == '\0') return (*iter);
    if(!strncasecmp(*iter, "if", 2))
    {
        (*iter)+=2;
        *iter = if_parser(iter,string);
    } else if (!strncasecmp(*iter, "put", 3)) {
        (*iter)+=3;
        *iter = put_parser(iter,string);
    }
    else{
        printf("Failed '%u'\n", (**iter));
    }
    /* skip over */
    (*iter)++;

    return *iter;
}

void parse_string(GString *string, mpd_Song *song, char *markup)
{
    char *iter;
    char buffer[4096];
    mpd_song_markup(buffer, 4096, markup, song);
    iter = buffer;

    while(*iter)
    {
        iter = parser(&iter,string);
    }
}
